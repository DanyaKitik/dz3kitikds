<?php
require_once 'db.php';
$row = null;
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $sql = "update work_experience set start_date=:start_date, end_date=:end_date,company=:company,position=:position,description=:description where id = :id";
    $sth = $dbh->prepare($sql);
    $sth ->execute([
        ':start_date'=>$_POST['start_date'],
        ':end_date'=>$_POST['end_date'],
        ':company'=>$_POST['company'],
        ':position'=>$_POST['position'],
        ':description'=>$_POST['description'],
        ':id'=>$_POST['id'],
    ]);
    header('Location:list.php');
}else{
    $id = $_GET['id'];
    $sql = "select * from work_experience where id = ?";
    $stmt = $dbh->prepare($sql);
    $stmt->execute([$id]);
    $row = $stmt->fetch();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="" method="post">
    <input type="date" name="start_date" value="<?= $row['start_date']?>">
    <input type="date" name="end_date" value= "<?= $row['end_date']?>">
    <input type="text" name="company" value= "<?= $row['company']?>">
    <input type="text" name="position" value= "<?= $row['position']?>">
    <input type="text" name="description" value= "<?= $row['description']?>">
    <input type="hidden" name="id" value="<?= $row['id']?>">
    <input type="submit" value="Update">
</form>
</body>
</html>

