<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    require_once 'db.php';
    $sql = "insert into
    work_experience (id,start_date, end_date, company, position, description)
    value (NULL,:start_date,:end_date,:company,:position,:description)";
    $sth = $dbh->prepare($sql);
    $sth ->execute([
            ':start_date'=>$_POST['start_date'],
            ':end_date'=>$_POST['end_date'],
            ':company'=>$_POST['company'],
            ':position'=>$_POST['position'],
            ':description'=>$_POST['description'],
    ]);
    header('Location:list.php');
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="" method="post">
    <input type="date" name="start_date">
    <input type="date" name="end_date">
    <input type="text" name="company">
    <input type="text" name="position">
    <input type="text" name="description">
    <input type="submit" value="create">
</form>
</body>
</html>
