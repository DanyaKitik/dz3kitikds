<?php
require_once 'db.php';

$sth = $dbh->query("select * from work_experience;");


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p><a href="index.php">Main Page</a></p>
<p><a href="create.php">Create new Work Experiences</a></p>
    <table border="1">
        <tr>
            <th>id</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Company</th>
            <th>Position</th>
            <th>Description</th>
            <th>actions</th>
        </tr>
        <?php foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $item):?>
        <tr>
            <td><?= $item['id']?></td>
            <td><?= $item['start_date']?></td>
            <td><?= $item['end_date']?></td>
            <td><?= $item['company']?></td>
            <td><?= $item['position']?></td>
            <td><?= $item['description']?></td>
            <td><a href="del.php?id=<?= $item['id']?>">DEL</a><a href="update.php?id=<?= $item['id']?>">Upd</a></td>
        </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>
